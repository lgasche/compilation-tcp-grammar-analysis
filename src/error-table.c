#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error-table.h"

Error_table allocate_error_table(char* ident, int line, Error_type type) {
  Error_table tmp = (Error_cell*)malloc(sizeof(Error_cell));
  if (tmp == NULL) {   
    fprintf(stderr, "Error allocate cell.\n");
    return NULL;
  }
  strcpy(tmp->ident, ident);
  tmp->line = line;
  tmp->error_type = type;
  tmp->next = NULL;
  return tmp;
}

void append_error_table(Error_table *error_table, Error_cell* error_cell) {
  Error_table tmp = *error_table;
  while(tmp->next != NULL) {
    tmp = tmp->next;
  }
  tmp->next = error_cell;
}

void free_error_table (Error_table *lst) {
  Error_table tmp;
  while ((*lst) != NULL) {
    tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
  *lst = NULL;
}

void print_error_cell(Error_cell error_cell) {
  switch (error_cell.error_type) {
    case UNDECLARED_VAR:
      fprintf(stderr, "UNDECLARED_VAR %d %s\n", error_cell.line, error_cell.ident);
      break;
    case UNDECLARED_FUNCTION:
      fprintf(stderr, "UNDECLARED_FUNCTION %d %s\n", error_cell.line, error_cell.ident);
      break;
  }
}

void print_error_table(Error_table lst) {
  while (lst != NULL) {
    print_error_cell(*lst);
    lst =  lst->next;
  }
}