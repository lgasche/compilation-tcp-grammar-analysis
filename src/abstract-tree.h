/* abstract-tree.h */
#ifndef __ABSTRACT_TREE__
#define __ABSTRACT_TREE__

typedef enum {
  Program,
  VarDeclList,
  VarDec,
  IntLiteral,
  CharLiteral,
  Identifier,
  DeclVars,
  Declarateurs,
  Type,
  Int,
  Char,
  VarDecList,
  DeclStructs,
  StructDec,
  DeclFoncts,
  DeclFonct,
  EnTeteFonct,
  Parametres,
  Param,
  Corps,
  SuiteInstr,
  Instr,
  Equal,
  Dif,
  Inf,
  Sup,
  InfEqual,
  SupEqual,
  Add,
  Sub,
  Div,
  Mod,
  Mul,
  Neg,
  Assign,
  Reade,
  Readc,
  Print,
  If,
  Else,
  While,
  Return,
  Or,
  And,
  Arguments,
  Exp,
  Field
} Kind;

typedef struct Node {
  Kind kind;
  union {
    int integer;
    char character;
    char identifier[64];
  } u;
  struct Node *firstChild, *nextSibling;
  int lineno;
} Node, *Tree ;

Node *makeNode(Kind kind);
void addSibling(Node *node, Node *sibling);
void addChild(Node *parent, Node *child);
void deleteTree(Node *node);
void printTree(Node *node);

#define FIRSTCHILD(node) node->firstChild
#define SECONDCHILD(node) node->firstChild->nextSibling
#define THIRDCHILD(node) node->firstChild->nextSibling->nextSibling


#endif