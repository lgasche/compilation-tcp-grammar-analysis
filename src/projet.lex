%option nounput
%option noinput
%option nodefault
%option noyywrap
%option yylineno

%{
#include "projet.tab.h"
%}

%%

"//".*\n?                           { /* Consumed. */ }
"/*"([^*]|\*[^/])*"*/"              { /* Consumed. */ }
[[:space:]]+                        { /* Consumed. */ }

char|int                            { strcpy(yylval.data, yytext); return TYPE; }
void                                { return VOID; } 
reade                               { return READE; }
readc                               { return READC; }
print                               { return PRINT; }
if                                  { return IF; }
else                                { return ELSE; }
while                               { return WHILE; }
return                              { return RETURN; }
struct                              { return STRUCT; }
[[:alpha:]][[:alnum:]_]{0,30}       { strcpy(yylval.data, yytext); return IDENT; }
"||"                                { strcpy(yylval.data, yytext); return OR; }
&&                                  { strcpy(yylval.data, yytext); return AND; }
[!=]=                               { strcpy(yylval.data, yytext); return EQ; }
[<>]=?                              { strcpy(yylval.data, yytext); return ORDER; }
[+-]                                { yylval.data[0] = yytext[0]; return ADDSUB; }
[/*%]                               { yylval.data[0] = yytext[0]; return DIVSTAR; }
[[:digit:]]+                        { yylval.num = atoi(yytext); return NUM; }
'([[:print:]]{-}[\\]|\\[\\'nt])'    {
                                      if (yytext[1] == '\\') {
                                        switch (yytext[2]) {
                                          case 't': yylval.data[0] = '\t'; break;
                                          case 'n': yylval.data[0] = '\n'; break;
                                          case '\'': yylval.data[0] = '\''; break;
                                          case '\\': yylval.data[0] = '\\'; break;
                                        }
                                      }
                                      else {
                                        yylval.data[0] = yytext[1];
                                      }
                                      return CHARACTER;
                                    }
.                                   { return yytext[0]; }
<<EOF>>                             { YY_USER_ACTION; yyterminate(); }

%%
