%code {
#include <string.h>
#include "src/abstract-tree.h"
#include "src/symbol-table.h"
#include "src/error-table.h"

#include <stdio.h>
void yyerror(const char* msg);
int yylex();

Tree tree;
int count = 0;
extern int yylineno;
}

%token TYPE
%token IDENT
%token VOID
%token READE
%token READC
%token PRINT
%token IF
%token ELSE
%token WHILE
%token RETURN
%token OR
%token AND
%token EQ
%token ORDER
%token ADDSUB
%token DIVSTAR
%token NUM
%token CHARACTER
%token STRUCT

%right ')' ELSE

%union {
  char data[64];
  int num;
  struct Node *node;
}

%type <data> TYPE IDENT ADDSUB ORDER EQ DIVSTAR NUM CHARACTER
%type <node> Prog DeclStructs DeclVars DeclFoncts DeclFonct Type Declarateurs EnTeteFonct Parametres 
ListTypVar Corps SuiteInstr Instr LValue Exp E F T M FB TB ListExp Arguments

%%

Prog : 
      DeclStructs DeclVars DeclFoncts     { 
                                            
                                            $$ = makeNode(Program);
                                            
                                            /* DeclStructs. */
                                            Node* declStructs = makeNode(DeclStructs);
                                            addChild(declStructs, $1);
                                            addChild($$, declStructs);
                                            
                                            /* DeclVars. */
                                            Node* declVars = makeNode(DeclVars);
                                            addChild(declVars, $2); /* Add variables in declVars. */
                                            addChild($$, declVars); /* Add declVars at program. */                                         

                                            /* DeclFoncts. */
                                            Node* declFoncts = makeNode(DeclFoncts);
                                            addChild(declFoncts, $3);
                                            addChild($$, declFoncts);

                                            tree = $$;
                                          }
     | DeclStructs DeclFoncts             {
                                            
                                            $$ = makeNode(Program);
                                            
                                            /* DeclStructs. */
                                            Node* declStructs = makeNode(DeclStructs);
                                            addChild(declStructs, $1);
                                            addChild($$, declStructs);                                      

                                            /* DeclFoncts. */
                                            Node* declFoncts = makeNode(DeclFoncts);
                                            addChild(declFoncts, $2);
                                            addChild($$, declFoncts);

                                            tree = $$;
                                          }
     ;

DeclStructs : DeclStructs STRUCT IDENT '{' DeclVars '}' ';' { 
  
                                                              $$ = makeNode(StructDec);
                                                                       
                                                              Node* ident = makeNode(Identifier);
                                                              strcpy(ident->u.identifier, $3);
                                                              addChild($$, ident);
                                                              
                                                              Node* declVars = makeNode(DeclVars);
                                                              addChild(declVars, $5);
                                                              addChild($$, declVars);

                                                              if ($1 !=  NULL) {  /* Var dans l'ordre. */
                                                                addSibling($1, $$);
                                                                $$ = $1;
                                                              }
                                                            }
            | %empty     {  $$ = NULL;}                                  
            ;

DeclVars : DeclVars Type Declarateurs ';' { 
  
                                            Node* brother = makeNode(VarDec);
                                            
                                            addSibling($1, brother); /* order var. */
                                            addChild(brother, $2);
                                            addChild(brother, $3);
                                            $$ = $1;
                                          }

         | Type Declarateurs ';'          {
           
                                            $$ = makeNode(VarDec);
                                            addChild($$, $1);
                                            addChild($$, $2);
                                          }                                      
         ;

Type : TYPE                               { 
  
                                            $$ = makeNode(Type);
                                            strcpy($$->u.identifier, $1);
                                          }
     | STRUCT IDENT                       {
       
                                            $$ = makeNode(Type);
                                            strcpy($$->u.identifier, $2);                                           
                                          }
     ;

Declarateurs : Declarateurs ',' IDENT     { 
  
                                            Node* brother = makeNode(Identifier);
                                            
                                            strcpy(brother->u.identifier, $3);
                                            addSibling($1, brother);
                                            $$ = $1;
                                          }

             | IDENT                      { 
               
                                            $$ = makeNode(Identifier);
                                            
                                            strcpy($$->u.identifier, $1);
                                          }
             ;

DeclFoncts : DeclFoncts DeclFonct         { 
  
                                            $$ = $2;          /* LINK.*/
                                            addSibling($$, $1);
                                            
                                          }
           | DeclFonct                    { 
             
                                            $$ = $1; 
                                          }
           ;

DeclFonct : EnTeteFonct Corps             { 
  
                                            $$ = makeNode(DeclFonct);

                                            addChild($$, $1);
                                            addChild($$, $2); /* todo erreur type ici. */
                                          }
          ;

EnTeteFonct : Type IDENT '(' Parametres ')' {
  
                                              $$ = makeNode(EnTeteFonct);

                                              addChild($$, $1);

                                              Node* ident = makeNode(Identifier);
                                              strcpy(ident->u.identifier, $2);
                                              addChild($$, ident);

                                              /* TODO parametres. */
                                              addChild($$, $4);
                                            }
            | VOID IDENT '(' Parametres ')' {
              
                                              $$ = makeNode(EnTeteFonct);

                                              Node* type = makeNode(Type);
                                              strcpy(type->u.identifier, "void");
                                              addChild($$, type);

                                              Node* ident = makeNode(Identifier);
                                              strcpy(ident->u.identifier, $2);
                                              addChild($$, ident);

                                              addChild($$, $4);
                                            }
            ;

Parametres : VOID             {
  
                                $$ = makeNode(Parametres);

                                Node* type = makeNode(Type);
                                strcpy(type->u.identifier, "void");
                                addChild($$, type);

                                /* Todo faire un node param, même pour le cas void?. */

                              }
           | ListTypVar       {
             
                                $$ = makeNode(Parametres);

                                addChild($$, $1);
                              }
           ;

ListTypVar : ListTypVar ',' Type IDENT    {
  
                                            Node* brother = makeNode(Param);

                                            addSibling($1, brother);
                                            addChild(brother, $3);

                                            /* TODO faire une fonction IDENT. */
                                            Node* ident = makeNode(Identifier);
                                            strcpy(ident->u.identifier, $4);
                                            addChild(brother, ident);
                                            $$ = $1;
                                          }
           | Type IDENT                   {
             
                                            $$ = makeNode(Param);

                                            addChild($$, $1);

                                            Node* ident = makeNode(Identifier);
                                            strcpy(ident->u.identifier, $2);
                                            addChild($$, ident);
                                          }
           ;

Corps : '{' DeclVars SuiteInstr '}'       {
  
                                            $$ = makeNode(Corps);

                                            Node* declVars = makeNode(DeclVars);

                                            addChild(declVars, $2);
                                            addChild($$, declVars);

                                            Node* suiteInstr = makeNode(SuiteInstr);
                                            addChild(suiteInstr, $3);
                                            addChild($$, suiteInstr);
                                          }
      | '{' SuiteInstr '}'                {
        
                                            $$ = makeNode(Corps);
                                            Node* suiteInstr = makeNode(SuiteInstr);
                                            addChild(suiteInstr, $2);
                                            addChild($$, suiteInstr);
                                          }
      ;

SuiteInstr : SuiteInstr Instr             { 
  
                                            $$ = $2;
                                            if ($1 !=  NULL) {  
                                              addSibling($1, $$);
                                              $$ = $1;
                                            }
                                          }
           | %empty                       {
             
                                            $$ =NULL;
                                          }
           ;

Instr : LValue '=' Exp ';'                      { 
  
  $$ = makeNode(Assign);
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
      | READE '(' IDENT ')' ';'                 { 
        
        $$ =  makeNode(Reade);
                                                  Node* ident = makeNode(Identifier);
                                                  strcpy(ident->u.identifier, $3);
                                                  addChild($$, ident);
                                                }
      | READC '(' IDENT ')' ';'                 { 
        
        $$ =  makeNode(Readc);
                                                  Node* ident = makeNode(Identifier);
                                                  strcpy(ident->u.identifier, $3);
                                                  addChild($$, ident);
                                                }
      | PRINT '(' Exp ')' ';'                   { 
        
                                                 $$ = makeNode(Print);
                                                 addChild($$, $3); 
                                                }
      | IF '(' Exp ')' Instr                    { 
        
                                                  $$ = makeNode(If);
                                                  addChild($$,$3);
                                                  addChild($$,$5);
                                                }
      | IF '(' Exp ')' Instr ELSE Instr         { 
        
                                                  $$ = makeNode(If);
                                                  Node* elseNode = makeNode(Else);
                                                  addChild(elseNode, $7);
                                                  addChild($$, $3);
                                                  addChild($$, $5);
                                                  addSibling($$, elseNode);
                                                }
      | WHILE '(' Exp ')' Instr                 { 
        
                                                  $$ = makeNode(While);
                                                  addChild($$, $3);
                                                  addChild($$, $5);
                                                }
      | IDENT '(' Arguments ')' ';'             { 
        
                                                  $$ = makeNode(Identifier);
                                                  strcpy($$->u.identifier, $1);
                                                  addChild($$, $3);
                                                }
      | RETURN Exp ';'                          { 
        
                                                  $$ = makeNode(Return);
                                                  addChild($$, $2);
                                                }
      | RETURN ';'                              { 
        
                                                  $$ = makeNode(Return);
                                                }
      | '{' SuiteInstr '}'                      { 
        
                                                  $$ = makeNode(SuiteInstr);
                                                  addChild($$, $2);
                                                }
      | ';'                                     { 
        
                                                  $$ =  NULL;
                                                }
      ;

Exp : Exp OR TB                                 {
  
                                                  $$ = makeNode(Or);
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
    | TB                                        {
      
                                                  $$ = $1;
                                                }
    ;

TB : TB AND FB                                  {
  
                                                  $$ = makeNode(And);
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
   | FB                                         {
     
                                                  $$ = $1;
                                                }
   ;

FB : FB EQ M                                    {
  
                                                  if (strcmp($2, "==") == 0) {
                                                    $$ = makeNode(Equal);
                                                  }
                                                  else {
                                                    $$ = makeNode(Dif);
                                                  }
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
   | M                                          {
     
                                                  $$ = $1;
                                                }
   ;

M : M ORDER E                                   {
  
                                                  if (strcmp($2, "<") == 0) {
                                                    $$ = makeNode(Inf);
                                                  }
                                                  else if(strcmp($2, ">") == 0) {
                                                    $$ = makeNode(Sup);
                                                  }
                                                  else if(strcmp($2, "<=") == 0) {
                                                    $$ = makeNode(InfEqual);
                                                  }
                                                  else if(strcmp($2, ">=") == 0) {
                                                    $$ = makeNode(SupEqual);
                                                  }
                                                  /* TODO : check case null? */
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
  | E                                           {
    
                                                  $$ = $1;
                                                }
  ;

E : E ADDSUB T                                  {
                                                  if (strcmp($2, "+")) {
                                                    $$ = makeNode(Add);
                                                  } 
                                                  else {
                                                    $$ = makeNode(Sub);
                                                  }
                                                  addChild($$, $1);
                                                  addChild($$, $3);
                                                }
  | T                                           {
    
                                                  $$ = $1;
                                                }
  ;

T : T DIVSTAR F                                  {
  
                                                  switch ($2[0]) {
                                                    case '/': $$ = makeNode(Div); break;
                                                    case '%': $$ = makeNode(Mod); break;
                                                    case '*': $$ = makeNode(Mul); break;
                                                  }
                                                  addChild($$, $1);
                                                  addChild($$, $3);  
                                                }
  | F                                           {
    
                                                  $$ = $1;
                                                }
  ;

F : ADDSUB F                                  {
  
                                                if (strcmp($1, "+")) {
                                                  $$ = makeNode(Add);
                                                } 
                                                else {
                                                  $$ = makeNode(Sub);
                                                }
                                                addChild($$, $2);
                                              }
  | '!' F                                     {     
    
                                                $$ = makeNode(Neg);					
                                                addChild($$, $2);
                                              }
  | '(' Exp ')'                               {
    
                                                $$ = $2;
                                              }
  | NUM                                       {
    
                                                $$ = makeNode(IntLiteral);
                                                $$->u.integer = $1[0];
                                              }
  | CHARACTER                                 { 
    
                                                $$ = makeNode(CharLiteral);
                                                /* Take the 'char' after apostrophe. */
                                                $$->u.character = $1[0];
                                              }
  | LValue                                    {
    
                                                $$ = $1;
                                              }
  | IDENT '(' Arguments ')'                   { 
    
                                                $$ = makeNode(Identifier);
                                                strcpy($$->u.identifier, $1);
                                                addChild($$, $3);
                                              }
  ;

LValue : IDENT                                { 
  
                                                $$ = makeNode(Identifier);
                                                strcpy($$->u.identifier, $1);
                                              }
        | IDENT '.' IDENT                     {
                                                $$ = makeNode(Field);
                                                Node* ident = makeNode(Identifier);
                                                Node* field = makeNode(Identifier);
                                                strcpy(ident->u.identifier, $1);
                                                strcpy(field->u.identifier, $3);
                                                addChild($$, ident);
                                                addSibling(ident, field);
                                              }
       ;

Arguments : ListExp                           {
  
                                                $$ = makeNode(Arguments);
                                                addChild($$, $1);
                                              }
          | %empty                            {
            
                                                $$ = NULL;
                                              }
          ;

ListExp : ListExp ',' Exp                     {
  
                                                $$ = makeNode(Exp);
                                                addChild($$, $3);
                                                addSibling($1, $$);
                                                $$ = $1;
                                              }
        | Exp                                 {
          
                                                $$ = makeNode(Exp);
                                                addChild($$, $1);
                                              }
        ; 

%%

void printLine(int line){
  /**
   * Print line line from stdin file.
   */

  int lineno;
  char c;
  rewind(stdin);
  lineno = 1;
  /* Skip lines before error line. */
  while(lineno != line){ 
    if(getchar() == '\n') lineno += 1;
  }
  /* show error line. */
  do{                    
    c=getchar();
    if(c != EOF) fprintf(stderr, "%c",c);
  }while (c != '\n' && c != EOF);
}

void yyerror(const char *s){
  /**
   * When there is an error, 
   * we browse the file again to be able to display the whole line as well as the line number, 
   * the character number and an arrow.
   */
  
  int cpt = 1;
  
  /*code_error = 1; */
  fprintf(stderr, "%s line %d column %d\n", s, yylineno, CharLiteral);
  printLine(yylineno);
  
  /* affichage de la flèche */
  for(cpt = 1; cpt < CharLiteral; cpt++){
    fprintf(stderr, " ");
  }
  fprintf(stderr, "^\n");
  for(cpt = 1; cpt < CharLiteral; cpt++){
    fprintf(stderr, " ");
  }
  fprintf(stderr, "|\n");
}

int main() {
  int ret = yyparse();
  if (ret == 0) {
    /* Init. */
    Symbol_table symbol_table = allocate_symbol_table(0, "", "", 0, 0, 0, 0);
    Error_table error_table = allocate_error_table("", 0, 0);
    walk_abstract_tree(tree, &symbol_table, &error_table, 0, 0);
    /* Remove the 1st cell from each list (init cell). */
    symbol_table = symbol_table->next;
    error_table = error_table->next;
    /* Check if main exist. */
    if (check_main_exist(symbol_table) == 1) {
      Error_cell *error_cell = allocate_error_table("main", 0, UNDECLARED_FUNCTION);
      append_error_table(&error_table, error_cell);
    }
    /* Displays. */
    printTree(tree);
    print_symbol_table(symbol_table);
    print_error_table(error_table);
    /* Check errors  */
    if (error_table != NULL) {
      ret = 2;
    }
  }
  return ret;
}
