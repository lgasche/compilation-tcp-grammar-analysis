#!/bin/bash

MAKE_DIR="$(dirname "$0")"

cd $MAKE_DIR
rm result.txt
make all

test_syn(){
    "./bin/compil" < $1
    if [ $? = 0 ]; then
        echo "$? : $1 est conforme à la syntaxe." >> result.txt
    else
        echo "$? : $1 n'est pas conforme à la syntaxe." >> result.txt
    fi
}

test_sem(){
    "./bin/compil" < $1
    if [ $? = 0 ]; then
        echo "$? : $1 est conforme à la sémantique." >> result.txt
    else
        echo "$? : $1 n'est pas conforme à la sémantique." >> result.txt
    fi
}

# Good tests.
echo "Good tests." >> result.txt
for f in test/good/*tpc; do test_syn $f; done

# Syn-error tests.
echo "Syn-error tests." >> result.txt
for f in test/syn-err/*tpc; do test_syn $f; done

# Sem-error tests.
echo "Sem-error tests." >> result.txt
for f in test/sem-err/*tpc; do test_sem $f; done