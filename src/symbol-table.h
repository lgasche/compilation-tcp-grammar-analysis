#ifndef __SYMBOL_TABLE__
#define __SYMBOL_TABLE__
#include "error-table.h"
#include "abstract-tree.h"

/*#pragma once */
typedef enum {
  FONCTION, PARAMETER, VARIABLE, STRUCTURE, ASSIGN, RET
} Role;

typedef struct __symbol_cell {
  Role role;
  char ident[64];
  char type[64];
  int address;
  /* Var init or not. */
  int is_initialized;
  int fonction_index;
  int struct_index;
  struct __symbol_cell *next;
} Symbol_cell, *Symbol_table;

Symbol_table allocate_symbol_table(Role role, char* ident, char* type, int address, int is_initialized, int fonction_index, int struct_index);
void append_symbol_table(Symbol_table *symbol_table, Symbol_cell* symbol_cell);
void free_symbol_table(Symbol_table *lst);
void print_symbol_table(Symbol_table lst);
void walk_abstract_tree(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index, int struct_index);
void walk_struct_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int struct_index);
void walk_var_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int struct_index);
void walk_fonct_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index);
void walk_assign(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index);
void walk_ret(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index);

/**
 * Check if the var is initialized before the assign.
 * Return 0, if not return 1.
 */
int check_var_declared(Symbol_table symbol_table, Node *node, int fonction_index);

/**
 * Check if the function main exist.
 * Return 0, if not return 1.
 */
int check_main_exist(Symbol_table symbol_table);

/**
 * Check if the assign is correct.
 * Return 0, if not return 1.
 */
int check_var_assign(Symbol_table symbol_table, char *var, int fonction_index) ;

#endif