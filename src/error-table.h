#ifndef __ERROR_TABLE__
#define __ERROR_TABLE__

typedef enum {
  UNDECLARED_VAR, UNDECLARED_FUNCTION
} Error_type;

typedef struct __error_cell {
  Error_type error_type;
  char ident[64];
  int line;
  struct __error_cell *next;
} Error_cell, *Error_table;

Error_table allocate_error_table(char* ident, int line, Error_type type);
void append_error_table(Error_table *error_table, Error_cell* error_cell);
void free_error_table(Error_table *lst);
void print_error_cell(Error_cell error_cell);
void print_error_table(Error_table lst);

#endif