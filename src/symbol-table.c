#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symbol-table.h"

Symbol_table allocate_symbol_table(Role role, char* ident, char* type, int address, int is_initialized, int fonction_index, int struct_index) {
  Symbol_table tmp = (Symbol_cell*)malloc(sizeof(Symbol_cell));
  if(tmp == NULL){   
    fprintf(stderr, "Error allocate cell.\n");
    return NULL;
  }
  tmp->role = role;
  strcpy(tmp->ident, ident);
  strcpy(tmp->type, type);
  tmp->address = address;
  tmp->is_initialized = is_initialized;
  tmp->fonction_index = fonction_index;
  tmp->struct_index = struct_index;
  tmp->next = NULL;
  return tmp;
}

void append_symbol_table(Symbol_table *symbol_table, Symbol_cell* symbol_cell) {
  Symbol_table tmp = *symbol_table;
  while(tmp->next != NULL) {
    tmp = tmp->next;
  }
  tmp->next = symbol_cell;
}

void free_symbol_table(Symbol_table *lst) {
  Symbol_table tmp;
  while((*lst) != NULL){
    tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
  *lst = NULL;
}

void print_symbol_table(Symbol_table lst) {
  while (lst != NULL) {
    switch (lst->role) {
      case FONCTION: printf("FONCTION  ");break;
      case PARAMETER: printf("PARAMETER ");break;
      case VARIABLE: printf("VARIABLE  ");break;
      case STRUCTURE: printf("STRUCTURE ");break;
      case ASSIGN: printf("ASSIGN    ");break;
      case RET: printf("RET       ");break;
    }
    printf("%12s %12s %d %d %d %d\n", lst->ident, lst->type, lst->address, lst->is_initialized, lst->fonction_index, lst->struct_index);
    lst = lst->next;
  }
}

void walk_abstract_tree(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index, int struct_index) {
  if (tree == NULL) return;
  switch (tree->kind) {
    case Program:
      /* DeclStructs. */
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      /* DeclVars. */
      walk_abstract_tree(SECONDCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      /* DecFoncts : Scope global -> scope fonction. */
      walk_abstract_tree(THIRDCHILD(tree), symbol_table, error_table, fonction_index+1, struct_index);
      break;
    case DeclStructs:
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      break;
    case StructDec:
      walk_struct_dec(tree, symbol_table, error_table, struct_index);
      /* New struct. */
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index, struct_index+1);
      break;
    case DeclVars:
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      break;
    case VarDec:
      walk_var_dec(tree, symbol_table, error_table, struct_index);
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index, struct_index);
      break;
    case DeclFoncts:
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      break;
    case DeclFonct:
      /* EnTeteFonct. */
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      /* Corps. */
      walk_abstract_tree(SECONDCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      /* New function. */
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index+1, struct_index);
      break;
    case EnTeteFonct:
      walk_fonct_dec(tree, symbol_table, error_table, fonction_index);
      break;
    case Corps:
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      break;
    case SuiteInstr:
      walk_abstract_tree(FIRSTCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      break;
    case Assign:
      walk_assign(tree, symbol_table, error_table, fonction_index);
      walk_abstract_tree(SECONDCHILD(tree), symbol_table, error_table, fonction_index, struct_index);
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index, struct_index);
      break;
    case Add:
      //call add
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index, struct_index);
      break;
    case Return:
      walk_ret(tree, symbol_table, error_table, fonction_index);
      walk_abstract_tree(tree->nextSibling, symbol_table, error_table, fonction_index, struct_index);
      break;      
    default:
      break;
  }
}

void walk_struct_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int struct_index) {
  Symbol_cell* symbol_cell = allocate_symbol_table(STRUCTURE, FIRSTCHILD(tree)->u.identifier, "", 0, 0, 0, struct_index);
  append_symbol_table(symbol_table, symbol_cell);
  Tree tmp = SECONDCHILD(tree)->firstChild;
  int count = 0;
  while (tmp != NULL) {
    Tree tmp2 = SECONDCHILD(tmp);
    while (tmp2 != NULL) {
      Symbol_cell *cell = allocate_symbol_table(VARIABLE, tmp2->u.identifier, tmp->firstChild->u.identifier, count, 0, 0, struct_index);
      count++;
      append_symbol_table(symbol_table, cell);
      tmp2 = tmp2->nextSibling;
    }
    tmp = tmp->nextSibling;
  }
}

void walk_var_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int struct_index) {
  Tree tmp = SECONDCHILD(tree);
  int count = 0;
  while (tmp != NULL) {
    Symbol_cell *cell = allocate_symbol_table(VARIABLE, tmp->u.identifier, FIRSTCHILD(tree)->u.identifier, count, 0, 0, struct_index);
    count++;
    append_symbol_table(symbol_table, cell);
    tmp = tmp->nextSibling;
  }
}

void walk_fonct_dec(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index) {
  Tree tmp = tree;
  /* Init cell of function signature. */
  Symbol_cell *fonct_cell = allocate_symbol_table(FONCTION, SECONDCHILD(tmp)->u.identifier, FIRSTCHILD(tmp)->u.identifier, 0, 0, fonction_index, 0);
  append_symbol_table(symbol_table, fonct_cell);
  int count = 0;
  /* Init cell of function parameters. */
  Tree tmp2 = THIRDCHILD(tmp);
  while (tmp2 != NULL) {
    Tree tmp3 = FIRSTCHILD(tmp2);
    while (tmp3 != NULL) {
      if (strcmp(tmp3->u.identifier, "") == 0) {
        Symbol_cell *cell = allocate_symbol_table(PARAMETER, FIRSTCHILD(tmp3)->u.identifier, SECONDCHILD(tmp3)->u.identifier, count, 0, fonction_index, 0);
        count++;
        append_symbol_table(symbol_table, cell);
      }
      tmp3 = tmp3->nextSibling;
    }
    tmp2 = tmp2->nextSibling;
  }
}

void walk_assign(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index) {
  /* Check if the var dosen't exist. */
  if (check_var_declared(*symbol_table, FIRSTCHILD(tree), fonction_index) == 1) {
    Error_cell *error_cell = allocate_error_table(FIRSTCHILD(tree)->u.identifier, FIRSTCHILD(tree)->lineno, UNDECLARED_VAR);
    append_error_table(error_table, error_cell);
  }
}

/**
 * WIP
 * Allows you to check the assignment
 */ 
void walk_exp(Tree tree, Symbol_table *Symbol_table, Error_table *error_table, int fonction_index) {
  if (tree == NULL) return;
  switch (tree->kind) {
    case Equal: 
    break;
    case Dif: 
    break;
    case Inf: 
    break;
    case Sup: 
    break;
    case InfEqual: 
    break;
    case SupEqual: 
    break;
    case Add: 
    break;
    case Sub: 
    break;
    case Div: 
    break;
    case Mod: 
    break;
    case Mul: 
    break;
    case Neg: 
    break;
    case Or: 
    break;
    case And: 
    break;
    case IntLiteral:
    break;
    case CharLiteral:
    break;
    default: break;
  }
}

void walk_ret(Tree tree, Symbol_table *symbol_table, Error_table *error_table, int fonction_index) {
  Symbol_cell *cell = allocate_symbol_table(RET, FIRSTCHILD(tree)->u.identifier, "", 0, 0, fonction_index, 0);
  append_symbol_table(symbol_table, cell);

}

int check_var_declared(Symbol_table symbol_table, Node *node, int fonction_index) {
  Symbol_table start = symbol_table;
  char* name = (node->kind == Identifier? node: FIRSTCHILD(node))->u.identifier;
  while (symbol_table != NULL) {
    /* Check var exist. */
    if (strcmp(name, symbol_table->ident) == 0) {
      /* Define in the global scope or function scope. */
      if (symbol_table->fonction_index == 0 || symbol_table->fonction_index == fonction_index) {
        if (node->kind == Field) {
          while (start != NULL) {
            /* Check if the field exist. */
            if ((start->role == STRUCTURE) && (strcmp(start->ident, symbol_table->type) == 0)) {
              int struct_index = start->struct_index;
              start = start->next;
              while (start != NULL) {
                if (start->struct_index != struct_index) {
                  break;
                }
                if (strcmp(SECONDCHILD(node)->u.identifier, start->ident) == 0) {
                  return 0;
                }
                start = start->next;
              }
              return 1;
            }
            start = start->next;
          }
        }
        return 0;
      }
    }
    symbol_table = symbol_table->next;
  }
  return 1;
}

int check_main_exist(Symbol_table symbol_table) {
  while (symbol_table != NULL) {
    if (strcmp("main", symbol_table->ident) == 0) {
      if (symbol_table->role == FONCTION) {
        return 0;
      }
    }
    symbol_table = symbol_table->next;
  }
  return 1;
}

int check_var_assign(Symbol_table symbol_table, char *var, int fonction_index) {
  while (symbol_table != NULL) {
    if (strcmp("main", symbol_table->ident) == 0) {
      if (symbol_table->role == FONCTION) {
        return 0;
      }
    }
    symbol_table = symbol_table->next;
  }
  return 1;
}